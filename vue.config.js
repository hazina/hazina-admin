module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  publicPath: "/",
  pwa:{
    name: 'Hazina',
    themeColor: '#f5a72b',
    msTileColor: '#ffffff',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
  }
}