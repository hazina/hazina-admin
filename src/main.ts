import Vue from 'vue'
import App from './views/App/index.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueApexCharts from 'vue-apexcharts'
import Vuelidate from 'vuelidate'
import Toasted from 'vue-toasted'
import Axios from 'axios'

Vue.config.productionTip = false
Vue.use(Vuelidate);
Vue.use(Toasted);
Vue.use(VueApexCharts);
Vue.component('apexchart', VueApexCharts);

Vue.prototype.$http = Axios;
const token = localStorage.getItem('authToken')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer '+token
}
  
Vue.prototype.$http.defaults.headers.common['Haz-Authorization'] = 'Bearer K3r1WjgalH7Og4A0aHL1nV7bk0sTT09aoq7SnAAS2ir1XOH22GBgg3QU8CUs';
Vue.prototype.$http.defaults.baseURL = (process.env.NODE_ENV === "production") ? 'https://api.gethazina.com/v1' : 'https://hazinang.dev/v1';

// Vue.prototype.$http.defaults.headers.common['Haz-Authorization'] = 'Bearer amfmw136hC1RjhdZCamMeiN8x349utATvrcXWJo8vfUQRKHYnGGxJQLxQ9JI';
// Vue.prototype.$http.defaults.baseURL = 'https://hazinang.dev/v1';
 
new Vue({
  router,
  store,
  vuetify, 
  render: h => h(App)
}).$mount('#app')
