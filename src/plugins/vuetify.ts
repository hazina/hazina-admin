import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from "vuetify/lib/util/colors";

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdiSvg', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4',
        values: {
        }
    },
    theme: {
        themes: {
            light: {
                base: '#ccc',
                primary: '#f5a72b', // #E53935
                secondary: '#35a9e0', // #FFCDD2
                accent: colors.indigo.base, // #3F51B5,
                icon: '#77869E',
                background: '#f5f5f5',
            }
        }
    }
}); 
