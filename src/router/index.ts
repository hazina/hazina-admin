import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Home from "../views/Home/index.vue";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component:Home,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/brands',
        component:()=>import("../views/Brand/Base/Base.vue"),
        meta: {
            requiresAuth: true
        },
        children:[
            { 
                path: '/',
                name: 'BrandHome',
                component:()=>import("../views/Brand/Home/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'create',
                name: 'CreateBrand',
                component:()=>import("../views/Brand/Create/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'edit/:id',
                name: 'EditBrand',
                component:()=>import("../views/Brand/Edit/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
        ]
    },
    {
    path: '/campaigns',
    component: ()=> import("../views/Campaign/Base/Base.vue"),
    meta: {
        requiresAuth: true
    },
    children:[
        {
            path: '',
            name: 'CampaignHome',
            component:()=>import("../views/Campaign/Home/index.vue"),
            meta: {
                requiresAuth: true
            } 
        },
        {
            path: 'create',
            name: 'CreateCampaign',
            component:()=> import("../views/Campaign/Create/index.vue"),
            meta: {
                requiresAuth: true
            }  
        },
        {
            path: 'edit/:id',
            name: 'EditCampaign',
            component:()=> import("../views/Campaign/Edit/index.vue"),
            meta: {
                requiresAuth: true
            } 
        },
        {
            path: 'view/:id', 
            name: 'ViewCampaign',
            component:()=> import("../views/Campaign/View/index.vue"),
            meta: {
                requiresAuth: true
            } 
        },
    ]
    },
    {
        path: '/consumers',
        component: ()=> import("../views/Consumer/Base/Base.vue"),
        meta: {
            requiresAuth: true
        },
        children:[
            {
                path: '',
                name: 'ConsumerHome',
                component:()=>import("../views/Consumer/Home/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'create',
                name: 'CreateConsumer',
                component:()=> import("../views/Consumer/Create/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'edit/:id',
                name: 'EditConsumer',
                component:()=> import("../views/Consumer/Edit/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
        ]
    },
    {
        path: '/daily-surveys',
        component: ()=> import("../views/DailySurvey/Base/Base.vue"),
        meta: {
            requiresAuth: true
        },
        children:[
            {
                path: '',
                name: 'DailySurveyHome',
                component:()=>import("../views/DailySurvey/Home/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'create',
                name: 'CreateDailySurvey',
                component:()=> import("../views/DailySurvey/Create/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'edit/:id',
                name: 'EditDailySurvey',
                component:()=> import("../views/DailySurvey/Edit/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
        ]
    },
    {
        path: '/feedback-surveys',
        component: ()=> import("../views/FeedbackSurvey/Base/Base.vue"),
        meta: {
            requiresAuth: true
        },
        children:[
            {
                path: '',
                name: 'FeedbackSurveyHome',
                component:()=>import("../views/FeedbackSurvey/Home/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'create',
                name: 'CreateFeedbackSurvey',
                component:()=> import("../views/FeedbackSurvey/Create/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'edit/:id',
                name: 'EditFeedbackSurvey',
                component:()=> import("../views/FeedbackSurvey/Edit/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
        ]
    },
    {
        path: '/freebies',
        component: ()=> import("../views/Freebie/Base/Base.vue"),
        meta: {
            requiresAuth: true
        },
        children:[
            {
                path: '',
                name: 'FreebieHome',
                component:()=>import("../views/Freebie/Home/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            }, 
            {
                path: 'create',
                name: 'CreateFreebie',
                component:()=> import("../views/Freebie/Create/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'edit/:id',
                name: 'EditFreebie',
                component:()=> import("../views/Freebie/Edit/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
        ]
    },
    {
        path: '/onboarding-surveys',
        component: ()=> import("../views/OnboardingSurvey/Base/Base.vue"),
        meta: {
            requiresAuth: true
        },
        children:[
            {
                path: '',
                name: 'OnboardingSurveyHome',
                component:()=>import("../views/OnboardingSurvey/Home/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'create',
                name: 'CreateOnboardingSurvey',
                component:()=> import("../views/OnboardingSurvey/Create/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'edit/:id',
                name: 'EditOnboardingSurvey',
                component:()=> import("../views/OnboardingSurvey/Edit/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
        ]
    },
    {
        path: '/pages',
        component: ()=> import("../views/Page/Base/Base.vue"),
        meta: {
            requiresAuth: true
        },
        children:[
            {
                path: '',
                name: 'PageHome',
                component:()=>import("../views/Page/Home/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'create',
                name: 'CreatePage',
                component:()=> import("../views/Page/Create/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'edit/:id',
                name: 'EditPage',
                component:()=> import("../views/Page/Edit/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
        ]
    },
    {
        path: '/insights',
        name: 'Insights',
        component: ()=> import('../views/Insights/Home/index.vue'),
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/category',
        component: ()=> import("../views/Category/Base/Base.vue"),
        meta: {
            requiresAuth: true
        },
        children:[
            {
                path: '',
                name: 'CategoryHome',
                component:()=>import("../views/Category/Home/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'create',
                name: 'CreateCategory',
                component:()=> import("../views/Category/Create/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'edit/:id',
                name: 'EditCategory',
                component:()=> import("../views/Category/Edit/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
        ]
    },
    {
        path: '/outbound-inventories',
        component:()=>import("../views/Inventory/Base/Base.vue"),
        meta: {
            requiresAuth: true
        },
        children:[
            { 
                path: '/',
                name: 'InventoryHome',
                component:()=>import("../views/Inventory/Home/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            }
        ]
    },
    {
        path: '/admin-user',
        component:()=>import("../views/Admin/Base/Base.vue"),
        meta: {
            requiresAuth: true
        },
        children:[
            { 
                path: '',
                name: 'AdminHome',
                component:()=>import("../views/Admin/Home/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            }, 
            {
                path: 'create',
                name: 'CreateAdmin',
                component:()=>import("../views/Admin/Create/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'edit/:id',
                name: 'EditAdmin',
                component:()=>import("../views/Admin/Edit/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
        ]
    },
    {
        path: '/banner-ads',
        component: ()=> import("../views/BannerAds/Base/Base.vue"),
        meta: { 
            requiresAuth: true
        },
        children:[  
            {
                path: '',
                name: 'BannerAdsHome',
                component:()=>import("../views/BannerAds/Home/index.vue"),
                meta: { 
                    requiresAuth: true
                } 
            },
            {
                path: 'create',
                name: 'CreateBannerAds',
                component:()=> import("../views/BannerAds/Create/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
            {
                path: 'edit/:id',
                name: 'EditBannerAds',
                component:()=> import("../views/BannerAds/Edit/index.vue"),
                meta: {
                    requiresAuth: true
                } 
            },
        ]
    },
    {
        path: '/login', 
        name: 'Login',
        component: ()=> import('../views/Login/index.vue')
    }
    

]

const router = new VueRouter({
    mode: 'history',
   // base: '/',
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next()
            return
        }
        next('/login')
    } else {
        next()
    }
});

export default router
