import {Component, Vue} from "vue-property-decorator";
import { required, minLength } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";

@Component({
  components: {},
  validations: {
    type:{required},
    //level:{required},
    amount:{required},
    points:{required},
    quantity:{required},
  }
})
export default class Edit extends Vue {
  isLoading: boolean = false;
  isFetching: boolean = false;
    types: any = [
        {
          text: "Airtime",
          value: "airtime"
        },
        {
          text: "Cash",
          value: "cash"
        },
        {
          text: "Coupon",
          value: "coupon"
        },
    ];

    levels: any = [];
    level: any = 1;
    amount: number = 1;
    points: number = 1;
    quantity: number = 1;
    type: string = "";

    submit(){
      this.$v.$touch();
      this.isLoading = true;
      if (this.$v.$invalid) {
       // this.$v.$touch();
        this.isLoading = false;
      }else{
        let data = this.$v;
      //  console.log(data);
        let formData = new FormData();
        //formData.append("level",data.level.$model);
        formData.append("amount",data.amount.$model);
        formData.append("points",data.points.$model);
        formData.append("type",data.type.$model);
        formData.append("quantity",data.quantity.$model);
        formData.append("_method", "PUT");
        axios.post("/admin/freebie/"+this.$route.params.id, formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
        }
        })
        .then((resp)=>{
          this.$toasted.success("Freebie updated successfully!").goAway(5000);
          this.isLoading = false;
          this.$router.push({name: "FreebieHome"});
        })
        .catch((err)=>{
          this.isLoading = false;
          this.$toasted.error("Error creating freebie!").goAway(5000);
        })
      }
    }

    mounted(){
      for(var n=1; n < 51; ++n){
        this.levels.push(n);
       }
       this.fetchData();
    }

    fetchData(){
      this.isFetching = true;
      axios.get("/admin/freebie/"+this.$route.params.id)
      .then((resp)=>{
        let data = resp.data.data;
        this.isFetching = false;
        //this.level = parseInt(data.level);
        this.amount = data.amount;
        this.points = data.points;
        this.quantity = data.quantity;
        this.type = data.type;
      })
      .catch((err)=>{
        this.isFetching = false;
        this.$toasted.error("Error fetching freebie!").goAway(5000);
      })
    }

    get typeErrors(){
      const errors: Array<string> = [];
        if (!this.$v.type.$dirty) return errors;
        !this.$v.type.required &&
            errors.push("Type is required.");
        return errors;
    }

    get levelErrors(){
      const errors: Array<string> = [];
        if (!this.$v.level.$dirty) return errors;
        !this.$v.level.required &&
            errors.push("Level is required.");
        return errors;
    }

    get amountErrors(){
      const errors: Array<string> = [];
        if (!this.$v.amount.$dirty) return errors;
        !this.$v.amount.required &&
            errors.push("Amount is required.");
        return errors;
    }

    get pointsErrors(){
      const errors: Array<string> = [];
        if (!this.$v.points.$dirty) return errors;
        !this.$v.points.required &&
            errors.push("Points is required.");
        return errors;
    }

    get quantityErrors(){
      const errors: Array<string> = [];
        if (!this.$v.quantity.$dirty) return errors;
        !this.$v.quantity.required &&
            errors.push("Quantity is required.");
        return errors;
    }

}

