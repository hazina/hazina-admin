import {Component, Vue} from "vue-property-decorator";
import { required, minLength, sameAs, email } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash"; 

@Component({
  components: {},
  validations: {
    username:{required, minLength: minLength(4)},
    password:{minLength: minLength(4)},
    email: { required, email },
    confirm_password:{minLength: minLength(4), sameAs: sameAs("password")},
  }
})
export default class Edit extends Vue {
    isLoading = false;
    isFetching = false;
    username : any = ""
    password : any = ""
    confirm_password : any = ""
    email : any = ""

    submit(){
      this.$v.$touch();
      this.isLoading = true;
      if (this.$v.$invalid) {
       // this.$v.$touch();
        this.isLoading = false;
      }else{
        let data = this.$v;
      //  console.log(data);
        let formData = new FormData();
        formData.append("username",data.username.$model);
        formData.append("password",data.password.$model);
        formData.append("email",data.email.$model);
        formData.append("_method", 'PUT');
        //console.log(formData);
        axios.post("/admin/admin-user/"+this.$route.params.id, formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
        }
        })
        .then((resp: any)=>{
          this.isLoading = false;
          this.$toasted.success("Admin created successfully!").goAway(5000);
          this.$router.push({name: "AdminHome"});
        })
        .catch((err: any)=>{
          this.isLoading = false;
          if (
            _.has(err.response.data, "errors") &&
            typeof err.response.data.errors != "string"
        ) {
            var field = _.keys(err.response.data.errors);
            this.$toasted.error(err.response.data.errors[field[0]][0]).goAway(5000); 
        } else {
            this.$toasted.error(err.response.data.errors).goAway(5000); 
        }

        })
      }
    }
    
    getData(){
      this.isFetching = true;
      axios.get("/admin/admin-user/"+this.$route.params.id)
      .then((resp: any)=>{
        let data = resp.data.data
        this.username = data.username;
        this.email = data.email;
        this.isFetching = false;
      })
      .catch((err)=>{
        this.isFetching = false;
        this.$toasted.error("Error fetching admin!").goAway(5000);
      })
    }

    mounted(){
      this.getData();
    }

    get usernameErrors(){
      const errors: Array<string> = [];
        if (!this.$v.username.$dirty) return errors;
        !this.$v.username.minLength &&
            errors.push("username must be at least 4 characters long");
        !this.$v.username.required &&
            errors.push("username is required.");
        return errors;
    }

    get passwordErrors() {
      const errors: Array<string> = [];
      if (!this.$v.password.$dirty) return errors;
      // !this.$v.password.required && errors.push("Password is required");
      !this.$v.password.minLength &&
          errors.push("Password must be at least 4 characters long");
      //!this.$v.password.sameAs && errors.push('Passwords do not match')
      return errors;
  }
  get confirmPasswordErrors() {
      const errors: Array<string> = [];
      if (!this.$v.confirm_password.$dirty) return errors;
      // !this.$v.confirm_password.required &&
      //     errors.push("You must confirm your password.");
      !this.$v.confirm_password.minLength &&
          errors.push("Password must be at least 4 characters long");
      !this.$v.confirm_password.sameAs &&
          errors.push("Passwords do not match");
      return errors;
  }

    get emailErrors() {
      const errors: Array<string> = [];
      if (!this.$v.email.$dirty) return errors;
      !this.$v.email.email && errors.push("Must be valid e-mail");
      !this.$v.email.required && errors.push("E-mail is required");
      return errors;
  }

}

