import { Component, Prop, Vue } from 'vue-property-decorator';
import * as SurveyCreator from "survey-creator";
import { required, minLength, email } from "vuelidate/lib/validators";
import "survey-creator/survey-creator.css";
import * as SurveyKo from "survey-knockout";
import Axios from 'axios';
import * as _ from "lodash"

@Component({
    validations: {
        survey_name: { required, minLength: minLength(6) },
        campaign: { required },
        ///photo: {required}
    }
})
export default class Edit extends Vue {
    survey: string = "";
    survey_name: string = "";
    campaign: number = 0;
    surveyCreator: any;
    isLoading: boolean = false;
    surveyEmpty: boolean = false;
   // photo: any = [];
    campaigns: any = [];

    mounted() {
      this.fetchData();
       let questionTypes = ["checkbox", "radiogroup", "dropdown", "rating", "imagepicker", "boolean"];

        this.surveyCreator = new SurveyCreator.SurveyCreator(
            "surveyCreatorContainer",
            {
                showState: true,
                questionTypes: questionTypes,
                showPagesToolbox: false,
                showPagesInTestSurveyTab: false,
                showSurveyTitle: "never"
            }
        );
        SurveyCreator.StylesManager.applyTheme("orange");
    }

    submitSurvey() {
        this.survey = this.surveyCreator.text;
        this.$v.$touch();
         this.isLoading = true;
        if (this.$v.$invalid) {
            this.isLoading = false;
        } else {
            let formData = new FormData();
            formData.append("title", this.survey_name)
            formData.append("survey", this.survey)
            formData.append("campaign_id", <any>this.campaign)
            formData.append("_method", "PUT")

            let counter = 0;
            //count Questions
            _.forEach(JSON.parse(this.survey)['pages'], (page, index)=>{
                if(_.has(page, 'elements')){
                    counter += page['elements'].length;
                }
            });
            formData.append("questions", <any>counter);

            Axios.post("admin/feedback-survey/"+this.$route.params.id, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                  }
            })
            .then((resp: any)=>{
                this.isLoading = false;
                this.surveyEmpty = false;
                this.$toasted.success("Survey created successfully!").goAway(1000)
                this.$router.push({name: "FeedbackSurveyHome"});
            })
            .catch((error: any)=>{
                this.$toasted.error(error.response.data.errors[0]).goAway(1000)
                this.surveyEmpty = false;
                this.isLoading = false;

            });
        }
    }

    fetchData(){
      this.isLoading = true;
      Axios.get('admin/feedback-survey/'+this.$route.params.id)
      .then((resp)=>{
        this.campaigns = resp.data.campaigns
        this.campaign = resp.data.data.campaign_id;
        this.survey = resp.data.data.survey.survey;
        this.surveyCreator.text = resp.data.data.survey.survey;
        this.survey_name = resp.data.data.survey.title;
        this.isLoading = false;
      })
      .catch((err)=>{
        this.isLoading = false;
        this.$toasted.error("error fetching campaigns").goAway(1000);
      });
    }

    get surveyNameErrors() {
        var errors: Array<string> = [];
        if (!this.$v.survey_name.$dirty) return errors;
        !this.$v.survey_name.minLength &&
            errors.push("Survey Name cannot be less than 4 characters");
        !this.$v.survey_name.required && errors.push("Survey Name is required");
        return errors;
    }

    get campaignErrors() {
        var errors: Array<string> = [];
        if (!this.$v.campaign.$dirty) return errors;
        !this.$v.campaign.required && errors.push("campaign is required");
        return errors;
    }

    // get photoErrors() {
    //     var errors: Array<string> = [];
    //     if (!this.$v.photo.$dirty) return errors;
    //     !this.$v.photo.required && errors.push("Photo is required");
    //     return errors;
    // }
}
