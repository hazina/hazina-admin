import axios from 'axios';
import {Component, Vue, Watch} from "vue-property-decorator";

@Component({
  components: {}
})
export default class Home extends Vue {
  data: any = [];
  isLoading:boolean = false;

  mounted(){
    this.fetchData();
  }

  fetchData(){
    axios.get("/admin/outbound/all")
    .then((resp:any)=>{
      this.data = resp.data.data;
    })
    .catch((err:any)=>{
      this.$toasted.error("Error fetching data").goAway(500);
    })
  }

}

 