import { Component, Vue } from "vue-property-decorator";
import * as SurveyCreator from "survey-creator";
import { required, minLength } from "vuelidate/lib/validators";
import "survey-creator/survey-creator.css";
import * as SurveyKo from "survey-knockout";
import Axios from 'axios';
import * as _ from "lodash"


@Component({
    validations: {
        survey_name: { required, minLength: minLength(6) }
    }
})
export default class EditOnboardingSurvey extends Vue {
    survey: string = "";
    survey_name: string = "";
    surveyCreator: any;
    isLoading: boolean = false;
    surveyEmpty: boolean = false;
    photo: any = [];
    old_photo: any = "";

    mounted() {
      let questionTypes = ["checkbox", "radiogroup", "dropdown", "rating", "imagepicker", "boolean"];
        this.surveyCreator = new SurveyCreator.SurveyCreator(
            "surveyCreatorContainer",
            {
                showState: true,
                questionTypes: questionTypes,
                showPagesToolbox: false,
                showPagesInTestSurveyTab: false,
                showSurveyTitle: "never"
            }
        );
        SurveyCreator.StylesManager.applyTheme("orange");
        this.fetchSurvey();
    }
 
    fetchSurvey(){
        this.isLoading = true;
        Axios.get("/admin/onboarding-survey/"+ this.$route.params.id)
        .then((resp: any)=>{
            this.isLoading = false;
            this.survey = resp.data.data.survey.survey;
            this.surveyCreator.text = resp.data.data.survey.survey;
            this.survey_name = resp.data.data.survey.title;
            this.old_photo = resp.data.data.survey.photo;
        })
        .catch((error)=>{
            this.isLoading = false;
            this.$toasted.error("error fetching survey").goAway(1500)
        })
    }

    submitSurvey() {
        this.survey = this.surveyCreator.text;
        this.$v.$touch();
        this.isLoading = true;
        if (this.$v.$invalid) {
            this.isLoading = false;
        } else {
            let formData = new FormData();
            formData.append("title", this.survey_name)
            formData.append("survey", this.survey)
            // formData.append("photo", this.photo)

            let counter = 0;
            // count Questions
            _.forEach(JSON.parse(this.survey)['pages'], (page, index)=>{
                if(_.has(page, 'elements')){
                    counter += page['elements'].length;
                }
            });
            formData.append("questions", <any>counter);
            formData.append("_method", "PUT");

            Axios.post("/admin/onboarding-survey/"+ this.$route.params.id, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                  }
            })
            .then((resp: any)=>{
                this.isLoading = false;
                this.surveyEmpty = false;
                this.$toasted.success("Survey created successfully!").goAway(1500)
                this.$router.push({name: "OnboardingSurveyHome"});
            })
            .catch((error: any)=>{
                this.$toasted.error(error.response.data.errors[0]).goAway(1500)
                this.isLoading = true;
                this.surveyEmpty = false;
                this.isLoading = false;

            });
        }
    }

    get surveyNameErrors() {
        var errors: Array<string> = [];
        if (!this.$v.survey_name.$dirty) return errors;
        !this.$v.survey_name.minLength &&
            errors.push("Survey Name cannot be less than 4 characters");
        !this.$v.survey_name.required && errors.push("Survey Name is required");
        return errors;
    }

    get photoErrors() {
        var errors: Array<string> = [];
        if (!this.$v.photo.$dirty) return errors;
        !this.$v.photo.required && errors.push("Photo is required");
        return errors;
    }
}
