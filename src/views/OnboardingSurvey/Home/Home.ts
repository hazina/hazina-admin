import { Component, Prop, Vue } from "vue-property-decorator";
import Axios from "axios";

@Component
export default class Home extends Vue {
    isLoading: boolean = false;
    surveys: Array<object> = [];
    currentPage: number = 1;
    totalPages: number = 1;

    Deletedialog = false;
    Datadeleting = false;
    deletedItem = {};
    cancelDisabled = false;

    fetchData() {
        this.getDataFromApi()
            .then((resp: any) => {
                let data = resp.data;
                this.surveys = data.data;
            })
            .catch(error => {
                this.$toasted.error("error fetching surveys").goAway(1500);
            });
    }

    openDeleteDialog(item: any) {
        this.deletedItem = { title: item.survey.title, id: item.id };
        this.Deletedialog = true;
    }

    deleteData() {
        var item: any = this.deletedItem;
        this.Datadeleting = true;
        this.cancelDisabled = true;
        //console.log(item.id);
        Axios.post("/admin/onboarding-survey/" + item.id, {_method: "DELETE"})
            .then(resp => {
                this.Datadeleting = false;
                this.Deletedialog = false;
                this.cancelDisabled = false;
                this.fetchData();
            })
            .catch(error => {
                this.$toasted.error("Unable to delete data");
                this.Datadeleting = false;
                this.Deletedialog = false;
                this.cancelDisabled = false;
            });
    }

    Paginate(page: any) {
        this.currentPage = page;
        this.fetchData();
    }

    getDataFromApi() {
        this.isLoading = true;
        return new Promise((resolve, reject) => {
            Axios.get("/admin/onboarding-survey", {
                params: { page: this.currentPage }
            })
                .then((resp: any) => {
                    let data = resp.data.data;
                    let items = data;
                    const total = data.total;
                    this.currentPage = data.current_page;
                    this.totalPages = data.last_page;
                    resolve({ data: items, total_data: data.total });
                    this.isLoading = false;
                })
                .catch((err: any) => {
                    reject(err);
                });
        });
    }

    mounted() {
        this.fetchData();
    }
}
