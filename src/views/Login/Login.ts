import { Component, Prop, Vue } from "vue-property-decorator";
import { required, minLength, email } from "vuelidate/lib/validators";
import store from "@/store";
import axios from "axios";

@Component({
    validations: {
        password: { required, minLength: minLength(6) },
        email: { required, email }
    }
})
export default class Login extends Vue {
    isLoading: boolean = false;
    hasError: boolean = false;
    errorMessages: string = "";

    email: string = "";
    password: string = "";

    submit() {
        this.$v.$touch();
        this.isLoading = true;
        if (this.$v.$invalid) {
            this.isLoading = false;
        } else {
            let data = {
                email: this.email,
                password: this.password
            };
            let that = this;
            store
                .dispatch("login", data)
                .then(() => {
                    this.$toasted
                        .success("You have logged in successfully!")
                        .goAway(1500);
                    this.$router.push("/");
                })
                .catch(err => {
                    this.hasError = true;
                    this.errorMessages = err.response.data.errors;
                    this.isLoading = false;
                });
        }
    }



    get emailErrors() {
        var errors: Array<string> = [];
        if (!this.$v.email.$dirty) return errors;
        !this.$v.email.email && errors.push("Must be valid e-mail");
        !this.$v.email.required && errors.push("E-mail is required");
        return errors;
    }
    get passwordErrors() {
        var errors: Array<string> = [];
        if (!this.$v.password.$dirty) return errors;
        !this.$v.password.required && errors.push("Password is required");
        !this.$v.password.minLength &&
            errors.push("Password must be at least 6 characters long");
        return errors;
    }
}
