import {Component, Vue} from "vue-property-decorator";
import { required, minLength } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";

@Component({
  components: {},
  validations: {
    name:{required, minLength: minLength(4)}
  }
})
export default class Create extends Vue {
    isLoading = false;
    
    name : any = ""

    submit(){
      this.$v.$touch();
      this.isLoading = true;
      if (this.$v.$invalid) {
       // this.$v.$touch();
        this.isLoading = false;
      }else{
        let data = this.$v;
      //  console.log(data);
        let formData = new FormData();
        formData.append("title",data.name.$model);
        axios.post("/admin/category", formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
        }
        })
        .then((resp)=>{
          this.$toasted.success("Category created successfully!").goAway(5000);
          this.isLoading = false;
          this.$router.push({name: "CategoryHome"});
        })
        .catch((err)=>{
          this.isLoading = false;
          this.$toasted.error("Error creating brand!").goAway(5000);
        })
      }
    }

    get nameErrors(){
      const errors: Array<string> = [];
        if (!this.$v.name.$dirty) return errors;
        !this.$v.name.minLength &&
            errors.push("name must be at least 4 characters long");
        !this.$v.name.required &&
            errors.push("name is required.");
        return errors;
    }


}

