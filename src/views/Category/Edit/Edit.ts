import {Component, Vue} from "vue-property-decorator";
import { required, minLength } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";

@Component({
  components: {},
  validations: {
    name:{required, minLength: minLength(4)}
  }
})
export default class Edit extends Vue {
    isLoading = false;
    isFetching = false;
    
    name : any = ""
    

    submit(){
      this.$v.$touch();
      this.isLoading = true;
      if (this.$v.$invalid) {
       // this.$v.$touch();
        this.isLoading = false;
      }else{
        let data = this.$v;
      //  console.log(data);
        let formData = new FormData();
        formData.append("title",data.name.$model);
        formData.append("_method", "PUT");
        //console.log(formData);
        axios.post("/admin/category/"+this.$route.params.id, formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
        }
        })
        .then((resp)=>{
          this.isLoading = false;
          this.$toasted.success("category updated successfully!").goAway(5000);
          this.$router.push({name: "CategoryHome"});
        })
        .catch((err)=>{ 
          this.isLoading = false;
          this.$toasted.error("Error updating category!").goAway(5000);
        })
      }
    }

    getData(){
      this.isFetching = true;
      axios.get("/admin/category/"+this.$route.params.id)
      .then((resp: any)=>{
        let data = resp.data.data
        this.name = data.title;
        this.isFetching = false;
      })
      .catch((err)=>{
        this.isFetching = false;
        this.$toasted.error("Error fetching category!").goAway(5000);
      })
    }

    mounted(){
      this.getData();
    }

    get nameErrors(){
      const errors: Array<string> = [];
        if (!this.$v.name.$dirty) return errors;
        !this.$v.name.minLength &&
            errors.push("name must be at least 4 characters long");
        !this.$v.name.required &&
            errors.push("name is required.");
        return errors;
    }


}

