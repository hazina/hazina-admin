import { Component, Prop, Vue } from "vue-property-decorator";
import Axios from "axios";
import store from "../../store";
import * as _ from "lodash";
import Spinner from "@/components/Spinner/Spinner.vue";

@Component({
    components: {
        Spinner
    }
})
export default class Home extends Vue {
    isLoading: boolean = false;
    data: any = [];

    consumer_series: Array<number> = [];
    samples_series: Array<number> = [];

    chartOptions: any = {
        chart: {
            type: "donut"
        },
        labels: ["Female", "Male", "Others"],
        responsive: [
            {
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: "bottom"
                    }
                }
            }
        ],
        colors:['#f5a72b', '#35a9e0', '#7c4d00']
    };

    samplesChartOptions: any = {
        chart: {
            type: "donut"
        },
        labels: ["Claimed", "Matched"],
        responsive: [
            {
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: "bottom"
                    }
                }
            }
        ],
        colors:['#f5a72b', '#35a9e0']
    };

    fetchData() {
        this.isLoading = true;
        Axios.get("/admin/dashboard")
            .then((resp: any) => {
                this.isLoading = false;
                this.data = resp.data.data;
                this.consumer_series.push(this.data.consumers.male, this.data.consumers.female, this.data.consumers.others);
                this.samples_series.push(this.data.samples.claimed, this.data.samples.matched);
            })
            .catch((err: any) => {
                this.isLoading = false;
                this.$toasted.error("error fetching data").goAway(10000);
            });
    }

    mounted() {
        this.fetchData();
    }

    get user() {
        return store.getters.user;
    }
}
