import { Component, Prop, Vue } from "vue-property-decorator";
import { required, minLength, sameAs, email } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";
import Spinner from "@/components/Spinner/Spinner.vue";

@Component({
    validations: {
        basicSetUpForm: {
            campaign_name: { required, minLength: minLength(4) },
            brand: { required },
            category: { required }
        },
        conditionForm: {
            country: { required },
            state: { required },
            gender: { required },
            age: { required },
            marital_status: { required },
            education: { required },
            employment: { required }
        },
        pageSetUpForm: {
            headlines: {
                $each: {
                    title: { required },
                    tagline: { required }
                }
            },
            features: {
                $each: {
                    title: { required },
                    description: { required }
                }
            },
            uploads: {
                banner: { required },
                pictures: { required }
            },
            product_story: {
                $each: {
                    story: { required }
                }
            },
            testimonials: {
                $each: {
                    headline: { required, minLength: minLength(4) },
                    testimonial: { required },
                    customer: { required }
                }
            },
            footer: {
                headline: { required, minLength: minLength(4) },
                body: { required },
                banner: { required }
            },
            time_and_sample: {
                sample: { required },
                duration: { required }
            }
        }
    },
    components: {
        Spinner
    }
})
export default class Create extends Vue {
    currentStep: number = 1;

    isLoading: boolean = false;
    isFormSubmitting: boolean = false;
    hasError: boolean = false;
    errorMessages: string = "";
    data: any = [];
    initMatchingTagsData : any = [];

    categories: any = [];
    selectedCategories: string = "";
    selectedColor: string = "";

    brands: Array<object> = [];
    matching_tags: Array<object> = [];
    selected_matching_tags: Array<object> = [];
    selected_matching_data: Array<object> = [];

    countries: Array<string> = [];
    states: Array<string> = [];

    gender: Array<string> = [];
    age: Array<string> = [];
    marital_status: Array<string> = [];
    education: Array<string> = [];
    employment: Array<string> = [];

    dateRange: any = ["2019-09-10", "2019-09-20"];
    Datemodal: boolean = false;

    //FORMS
    basicSetUpForm: object = {
        campaign_name: "",
        brand: "",
        category: ""
    };

    conditionForm: object = {
        country: [],
        state: [],
        gender: [],
        age: [],
        marital_status: [],
        education: [],
        employment: []
    };

    pageSetUpForm: object = {
        headlines: [
            {
                title: "",
                tagline: ""
            },
            {
                title: "",
                tagline: ""
            },
            {
                title: "",
                tagline: ""
            }
        ],
        features: [
            {
                title: "",
                description: ""
            },
            {
                title: "",
                description: ""
            },
            {
                title: "",
                description: ""
            }
        ],
        uploads: {
            banner: [],
            pictures: []
        },
        product_story: [
            {
                story: ""
            },
            {
                story: ""
            },
            {
                story: ""
            },
            {
                story: ""
            }
        ], 
        testimonials: [
            {
                headline: "",
                testimonials: "",
                customer: ""
            },
            {
                headline: "",
                testimonials: "",
                customer: ""
            },
            {
                headline: "",
                testimonials: "",
                customer: ""
            }
        ],
        footer: {
            headline: "",
            body: "",
            banner: []
        },
        time_and_sample: {
            sample: 0,
            duration: ["2019-09-10", "2019-09-20"]
        }
    };

    formData: Array<object> = [];


    getTitle(selected: any){
        let dd: any = _.find(this.matching_tags, {id: selected['id']});
        return dd['title'];
    }

    submitBasicSetUpForm() {
        this.$v.basicSetUpForm.$touch();
        let data = this.$v.basicSetUpForm.$model;
        data.category = this.selectedCategories;
        if (this.$v.basicSetUpForm.$invalid) {
            this.$v.basicSetUpForm.$touch();
        } else {
            this.currentStep = 2;
        }
    }

    submitConditionForm() {
        //this.$v.conditionForm.$touch();
        this.$v.conditionForm.$touch();
        let data = this.$v.conditionForm.$model;
        if (this.$v.conditionForm.$invalid) {
            this.$v.conditionForm.$touch();
        } else {
            this.currentStep = 3;
        }
    }

    testUp(){
      //console.log(this.$v.conditionForm.$model);
      this.currentStep = 3;
    }

    submitPageSetUpForm() {
        this.$v.pageSetUpForm.$touch();
        this.isFormSubmitting = true;
        if (this.$v.pageSetUpForm.$invalid) {
            this.$v.pageSetUpForm.$touch();
            this.isFormSubmitting = false;
        } else{
            let page = this.$v.pageSetUpForm.$model;
            let basic = this.$v.basicSetUpForm.$model;
            //let tags = this.$v.conditionForm.$model;

            // let setTags = [
            //     {id: this.initMatchingTagsData.country.id, value: tags.country},
            //     {id: this.initMatchingTagsData.state.id, value: tags.state},
            //     {id: this.initMatchingTagsData.demography[0].id, value: tags.age},
            //     {id: this.initMatchingTagsData.demography[1].id, value: tags.gender},
            //     {id: this.initMatchingTagsData.marital_status.id, value: tags.marital_status},
            //     {id: this.initMatchingTagsData.education.id, value: tags.education},
            //     {id: this.initMatchingTagsData.employment.id, value: tags.employment},
            // ]; 
            let formData = new FormData();
            //formData.append("_method", 'PUT');
            formData.append("brand", basic.brand);
            formData.append("campaign_name", basic.campaign_name);
            formData.append("status", 'OPEN');
            formData.append("starts_on",page.time_and_sample.duration[0]);
            formData.append("ends_on", page.time_and_sample.duration[1]);
            formData.append("categories", basic.category);
            formData.append("matching_tag_data", <any>this.selected_matching_data);
            formData.append("total_products", page.time_and_sample.sample);
            formData.append("page", JSON.stringify(page));
            formData.append("page_banner", page.uploads.banner);

            formData.append("footer_banner", page.footer.banner);
            _.forEach(page.uploads.pictures, (file, key)=>{
                formData.append("page_pictures[]", file);
            });

           axios.post("/admin/campaign", formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
           })
           .then((resp: any)=>{
            this.isFormSubmitting = false; 
                this.$router.push({name: "CampaignHome"});
                this.$toasted.success("Campaign created successfully!").goAway(1000);
           })
           .catch((error)=>{
            this.isFormSubmitting = false;
                this.$toasted.error(error.error);
           });
        }
    }

    initPage() {
        this.isLoading = true;
        axios
            .get("/admin/campaign/load-create")
            .then((resp: any) => {
                //console.log(resp.data.data.brands);
                let data = resp.data.data;
                this.categories = data.categories;
                this.brands = resp.data.data.brands;
                this.matching_tags = data.matching_tags;
                // this.countries = JSON.parse(data.country.options);
                // this.states = JSON.parse(data.state.options);
                // this.age = JSON.parse(data.demography[0].options);
                // this.gender = JSON.parse(data.demography[1].options);
                // this.marital_status = JSON.parse(data.marital_status.options);
                // this.education = JSON.parse(data.education.options);
                // this.employment = JSON.parse(data.employment.options);

                this.isLoading = false;
                this.selectedCategories = this.categories[0].id;
                this.initMatchingTagsData = data;
            }) 
            .catch(error => {
                this.isLoading = false;
                this.$toasted.error("error fetching data!").goAway(1000);
            });
    }

    markedCategory(id: any) {
       // if (_.includes(this.selectedCategories, id)) {
        if (this.selectedCategories = id) {
            return true;
        } else {
            return false;
        }
    }

    toggleMarkCategory(id: any) {
        if (!_.includes(this.selectedCategories, id)) {
            this.selectedCategories = id;
        } else {
            //_.pull(this.selectedCategories, id);
            this.selectedCategories = "";
        }
        this.markedCategory(id);
    }

    mounted() {
        this.initPage();
    } 

    get campaignNameErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.basicSetUpForm.campaign_name.$dirty) return errors;
        !val.basicSetUpForm.campaign_name.minLength &&
            errors.push("campaign_name must be at least 4 characters long");
        !val.basicSetUpForm.campaign_name.required &&
            errors.push("campaign name is required.");
        return errors;
    }

    get brandErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.basicSetUpForm.brand.$dirty) return errors;
        !val.basicSetUpForm.brand.required && errors.push("brand is required.");
        return errors;
    }

    get categoryErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.basicSetUpForm.category.$dirty) return errors;
        !val.basicSetUpForm.category.required &&
            errors.push("category is required.");
        return errors;
    }

    get countryErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.conditionForm.country.$dirty) return errors;
        !val.conditionForm.country.required &&
            errors.push("country is required.");
        return errors;
    }

    get stateErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.conditionForm.state.$dirty) return errors;
        !val.conditionForm.state.required && errors.push("state is required.");
        return errors;
    }

    get genderErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.conditionForm.gender.$dirty) return errors;
        !val.conditionForm.gender.required &&
            errors.push("gender is required.");
        return errors;
    }

    get ageErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.conditionForm.age.$dirty) return errors;
        !val.conditionForm.age.required && errors.push("age is required.");
        return errors;
    }

    get marital_statusErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.conditionForm.marital_status.$dirty) return errors;
        !val.conditionForm.marital_status.required &&
            errors.push("marital status is required.");
        return errors;
    }

    get educationErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.conditionForm.education.$dirty) return errors;
        !val.conditionForm.education.required &&
            errors.push("education is required.");
        return errors;
    }

    get employmentErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.conditionForm.employment.$dirty) return errors;
        !val.conditionForm.employment.required &&
            errors.push("employment is required.");
        return errors;
    }

    get headlineErrors() {
        const errors: any = [];
        const valitrons: any = this.$v.pageSetUpForm.headlines;
        _.forEach(valitrons.$each.$iter, (val: any, index: any) => {
            let titleErrors: any = [];
            let taglineErrors: any = [];
            if (!val["title"]["$dirty"]) {
                titleErrors;
            }
            if (val["title"]["$dirty"] && !val["title"]["required"]) {
                titleErrors.push(
                    "Headline Title " + (parseInt(index) + 1) + " is required"
                );
            }
            if (!val["tagline"]["$dirty"]) {
                taglineErrors;
            }
            if (val["tagline"]["$dirty"] && !val["tagline"]["required"]) {
                taglineErrors.push(
                    "Headline Tagline " + (parseInt(index) + 1) + " is required"
                );
            }

            errors.push({ title: titleErrors, tagline: taglineErrors });
        });
        return errors;
    }

    get featuresErrors() {
        const errors: any = [];
        const valitrons: any = this.$v.pageSetUpForm.features;
        _.forEach(valitrons.$each.$iter, (val: any, index: any) => {
            let titleErrors: any = [];
            let descriptionErrors: any = [];
            if (!val["title"]["$dirty"]) {
                titleErrors;
            }
            if (val["title"]["$dirty"] && !val["title"]["required"]) {
                titleErrors.push(
                    "Features Title " + (parseInt(index) + 1) + " is required"
                );
            }
            if (!val["description"]["$dirty"]) {
                descriptionErrors;
            }
            if (
                val["description"]["$dirty"] &&
                !val["description"]["required"]
            ) {
                descriptionErrors.push(
                    "Features Description " +
                        (parseInt(index) + 1) +
                        " is required"
                );
            }

            errors.push({ title: titleErrors, description: descriptionErrors });
        });
        return errors;
    }

    get bannerUploadErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.pageSetUpForm.uploads.banner.$dirty) return errors;
        !val.pageSetUpForm.uploads.banner.required &&
            errors.push("banner is required.");
        return errors;
    }

    get picturesUploadErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.pageSetUpForm.uploads.pictures.$dirty) return errors;
        !val.pageSetUpForm.uploads.pictures.required &&
            errors.push("pictures is required.");
        return errors;
    }

    get productStoryErrors() {
        const errors: any = [];
        const valitrons: any = this.$v.pageSetUpForm.product_story;
        _.forEach(valitrons.$each.$iter, (val: any, index: any) => {
            let storyErrors: any = [];
            if (!val["story"]["$dirty"]) {
                storyErrors;
            }
            if (val["story"]["$dirty"] && !val["story"]["required"]) {
                storyErrors.push(
                    "Product Story " + (parseInt(index) + 1) + " is required"
                );
            }

            errors.push({ story: storyErrors });
        });
        return errors;
    }

    get testimonialsErrors() {
        const errors: any = [];
        const valitrons: any = this.$v.pageSetUpForm.testimonials;
        _.forEach(valitrons.$each.$iter, (val: any, index: any) => {
            let headlineErrors: any = [];
            let customerErrors: any = [];
            let testimonialErrors: any = [];
            if (!val["headline"]["$dirty"]) {
                headlineErrors;
            }
            if (val["headline"]["$dirty"] && !val["headline"]["required"]) {
                headlineErrors.push(
                    "Testimonial headline " +
                        (parseInt(index) + 1) +
                        " is required"
                );
            }
            if (val["headline"]["$dirty"] && !val["headline"]["minLength"]) {
                headlineErrors.push(
                    "Testimonial headline " +
                        (parseInt(index) + 1) +
                        " must be at least 4 characters long"
                );
            }

            if (!val["customer"]["$dirty"]) {
                customerErrors;
            }
            if (val["customer"]["$dirty"] && !val["customer"]["required"]) {
                customerErrors.push(
                    "Customer " + (parseInt(index) + 1) + " is required"
                );
            }

            if (!val["testimonial"]["$dirty"]) {
                testimonialErrors;
            }
            if (
                val["testimonial"]["$dirty"] &&
                !val["testimonial"]["required"]
            ) {
                testimonialErrors.push(
                    "Testimonial " + (parseInt(index) + 1) + " is required"
                );
            }

            errors.push({
                headline: headlineErrors,
                customer: customerErrors,
                testimonial: testimonialErrors
            });
        });
        return errors;
    }

    get footerHeadlineErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.pageSetUpForm.footer.headline.$dirty) return errors;
        !val.pageSetUpForm.footer.headline.minLength &&
            errors.push("Footer Headline must be at least 4 characters long");
        !val.pageSetUpForm.footer.headline.required &&
            errors.push("Footer headline is required.");
        return errors;
    }

    get footerBodyErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.pageSetUpForm.footer.body.$dirty) return errors;
        !val.pageSetUpForm.footer.body.required &&
            errors.push("Footer body is required.");
        return errors;
    }

    get footerBannerErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.pageSetUpForm.footer.banner.$dirty) return errors;
        !val.pageSetUpForm.footer.banner.required &&
            errors.push("banner is required.");
        return errors;
    }

    get sampleErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.pageSetUpForm.time_and_sample.sample.$dirty) return errors;
        !val.pageSetUpForm.time_and_sample.sample.required &&
            errors.push("sample quantity is required.");
        return errors;
    }

    get durationErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.pageSetUpForm.time_and_sample.duration.$dirty) return errors;
        !val.pageSetUpForm.time_and_sample.duration.required &&
            errors.push("duraron is required.");
        return errors;
    }
}
