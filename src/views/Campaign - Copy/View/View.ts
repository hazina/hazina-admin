import { Component, Prop, Vue, Watch } from "vue-property-decorator";
import axios from "axios";
import * as _ from "lodash";

@Component({})
export default class View extends Vue {
    isLoading: boolean = false;
    campaign: any = {};

    getCampaign(){
        this.isLoading = true;
        axios.get("/admin/campaign/"+this.$route.params.id)
        .then((resp)=>{
            this.isLoading = false;
            this.campaign = resp.data.data;
        })
        .catch((err)=>{
            this.isLoading = false;
            this.$toasted.error("error fetching campaign").goAway(1000);
        })
    }

    mounted(){
        this.getCampaign();
    }
}   
 