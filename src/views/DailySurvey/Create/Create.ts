import { Component, Prop, Vue } from 'vue-property-decorator';
import * as SurveyCreator from "survey-creator";
import { required, minLength, email } from "vuelidate/lib/validators";
import "survey-creator/survey-creator.css";
import * as SurveyKo from "survey-knockout";
import Axios from 'axios';
import * as _ from "lodash"
const VImageInput = require("vuetify-image-input");
 
@Component({
    validations: {
        survey_name: { required, minLength: minLength(6) },
        level: { required },
        photo: {required}
    },
    components: {
        [VImageInput.name]: VImageInput,
    }, 
})
export default class Create extends Vue {
    survey: string = "";
    survey_name: string = "";
    level: number = 0;
    surveyCreator: any;
    isLoading: boolean = false;
    surveyEmpty: boolean = false;
    photo: any = [];
    levels: any = [];

    //imageDialog: boolean = false;

    mounted() {
        for(var n=1; n < 51; ++n){
            this.levels.push(n);
        }

       let questionTypes = ["checkbox", "radiogroup", "dropdown", "rating", "imagepicker", "boolean"];

        this.surveyCreator = new SurveyCreator.SurveyCreator(
            "surveyCreatorContainer",
            {
                showState: true,
                questionTypes: questionTypes,
                showPagesToolbox: false,
                showPagesInTestSurveyTab: false,
                showSurveyTitle: "never"
            }
        );
        SurveyCreator.StylesManager.applyTheme("orange");
    }

    submitSurvey() {
        this.survey = this.surveyCreator.text;
        this.$v.$touch();
         this.isLoading = true;
        if (this.$v.$invalid) {
            this.isLoading = false;
        } else {
            let formData = new FormData();
            formData.append("title", this.survey_name)
            formData.append("survey", this.survey)
            formData.append("level", <any>this.level)
           // if (this.photo.length) {
                formData.append("photo", this.photo);
             // }
            let counter = 0;
            //count Questions
            _.forEach(JSON.parse(this.survey)['pages'], (page, index)=>{
                if(_.has(page, 'elements')){
                    counter += page['elements'].length;
                }
            });
            formData.append("questions", <any>counter);

            Axios.post("admin/daily-survey", formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                  }
            })
            .then((resp: any)=>{
                this.isLoading = false;
                this.surveyEmpty = false;
                this.$toasted.success("Survey created successfully!").goAway(1000)
                this.$router.push({name: "DailySurveyHome"});
            })
            .catch((error: any)=>{
                this.$toasted.error(error.response.data.errors[0]).goAway(1000)
                this.surveyEmpty = false;
                this.isLoading = false;

            });
        }
    }

    get surveyNameErrors() {
        var errors: Array<string> = [];
        if (!this.$v.survey_name.$dirty) return errors;
        !this.$v.survey_name.minLength &&
            errors.push("Survey Name cannot be less than 4 characters");
        !this.$v.survey_name.required && errors.push("Survey Name is required");
        return errors;
    }

    get levelErrors() {
        var errors: Array<string> = [];
        if (!this.$v.level.$dirty) return errors;
        !this.$v.level.required && errors.push("level is required");
        return errors;
    }

    get photoErrors() {
        var errors: Array<string> = [];
        if (!this.$v.photo.$dirty) return errors;
        !this.$v.photo.required && errors.push("Photo is required");
        return errors;
    }

    dataURItoBlob(dataURI: any) {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        var byteString = atob(dataURI.split(",")[1]);
    
        // separate out the mime component
        var mimeString = dataURI
          .split(",")[0]
          .split(":")[1]
          .split(";")[0];
    
        // write the bytes of the string to an ArrayBuffer
        var ab = new ArrayBuffer(byteString.length);
    
        // create a view into the buffer
        var ia = new Uint8Array(ab);
    
        // set the bytes of the buffer to the correct values
        for (var i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }
    
        // write the ArrayBuffer to a blob, and you're done
        var blob = new Blob([ab], { type: mimeString });
        return blob;
      }

}
