import {Component, Vue, Watch} from "vue-property-decorator";
import axios from "axios";
import * as _ from "lodash";

@Component({
  components: {}
})
export default class Home extends Vue {
  search: any = "";

  Deletedialog = false;
  Datadeleting = false;
  deletedItem = {};
  cancelDisabled = false;

  //bulk delete
  bulkDeleteDialog = false;
  //bulkItems: any = [];
  bulkItemsDeleting: any = false;
  cancelDeletebulkItems: boolean = false;

  @Watch("options", { deep: true })
  selectedRows: any = [];

  totalData: number = 0;
  data: Array<object> = [];
  currentPage: number = 1;
  itemsPerPage: number = 0;
  loading: boolean = true;
  totalPages: number = 0;

  //options: any = {};
  headers: any = [
      {
          text: "Campaign",
          align: "start",
          sortable: true,
          //filterable: true,
          value: "campaign.title"
      },
    //   { text: "Created", value: "created_at" },
      { text: "Actions", value: "id", sortable: false, filterable: false }
  ];

  openDeleteDialog(item: any) {
      this.deletedItem = { title: item.name, id: item.id };
      this.Deletedialog = true;
  }

  deleteData() {
      var item: any = this.deletedItem;
      this.Datadeleting = true;
      this.cancelDisabled = true;
      axios
          .delete("/admin/page/delete", {params: {id: item.id}})
          .then(resp => {
              this.Datadeleting = false;
              this.Deletedialog = false;
              this.cancelDisabled = false;
              this.$toasted.success("Item deleted successfully!").goAway(5000);
              this.getCampaign();
          })
          .catch(error => { 
              this.$toasted.error("Unable to delete data").goAway(5000);
              this.Datadeleting = false;
              this.Deletedialog = false;
              this.cancelDisabled = false;
          });
  }

  openBulkItemsDialog() {
      this.bulkDeleteDialog = true;
  }

  deleteBulkItems() {
      this.bulkItemsDeleting = true;
      const IDS = _.map(this.selectedRows, "id");
      axios
          .post("/admin/page/bulk-delete", { ids: IDS })
          .then(resp => {
              this.bulkItemsDeleting = false;
              this.cancelDeletebulkItems = false;
              this.bulkDeleteDialog = false;
              this.$toasted.success("Item deleted successfully!").goAway(10000);
              this.getCampaign();
          })
          .catch(error => {
              this.bulkItemsDeleting = false;
              this.cancelDeletebulkItems = false;
              this.bulkDeleteDialog = false;
              this.$toasted.error("Unable to delete data").goAway(10000);
          });
  }

  getCampaign() {
      this.getDataFromApi()
          .then((resp: any) => {
              this.data = resp.data.data;
              // console.log(this.data);
          })
          .catch((error: any) => {
              this.$toasted.error("unable to fetch data").goAway(10000);
          });
  }

  Paginate(page: any) {
      this.currentPage = page;
      this.getCampaign();
  }

  updateBySearch() {
      this.getDataFromApi()
          .then((resp: any) => {
              this.data = resp.data.data;
              // console.log(this.data);
          })
          .catch((error: any) => {
              this.$toasted.error("unable to fetch data").goAway(10000);
          });
  }

  mounted() {
      this.getCampaign();
  }

  updateTable(options: any) {
      //console.log(options);
      const page = options.page;
      if (page > this.currentPage || page < this.currentPage) {
          this.currentPage = page;
          this.getDataFromApi()
              .then((resp: any) => {
                  this.data = resp.data.data;
                  // console.log(this.data);
              })
              .catch((error: any) => {
                  this.$toasted.error("unable to fetch data").goAway(10000);
              });
      }
  }

  getDataFromApi() {
      this.loading = true;
      return new Promise((resolve, reject) => {
          // const { sortBy, sortDesc, page, itemsPerPage } = this.options
          axios
              .get("/admin/page", {
                  params: { page: this.currentPage, search: this.search }
              })
              .then((resp: any) => {
                  let data = resp.data.data;
                  let items = data;
                  const total = data.total;
                  this.totalData = data.total;
                  this.currentPage = data.current_page;
                  this.itemsPerPage = data.per_page;
                  this.totalPages = data.last_page;
                  //console.log(data);
                  resolve({ data: items, total_data: data.total });
                  this.loading = false;
              })
              .catch(err => {
                  reject(err);
              });
      });
  }
}

 