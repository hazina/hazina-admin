import { Component, Prop, Vue, Watch } from "vue-property-decorator";
import { required, minLength, sameAs, email } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";
import Spinner from "@/components/Spinner/Spinner.vue";

@Component({
  components: {},
  validations: {
    pageSetUpForm: {
      product_name: { required },
      short_description: { required },
      long_description_title: { required },
      long_description_content: { required },
      features: {
        title: { required },
        feature_list: { required },
      },
      info_title: { required },
      info_content: { required },
      uploads: {
        banner: { required },
        pictures: { required },
      },
    },
  },
})
export default class Edit extends Vue {
  isLoading: boolean = false;
  isFormSubmitting: boolean = false;
  data: any = [];
  pageSetUpForm: any = {
    product_name: "",
    short_description: "",
    long_description_title: "",
    long_description_content: "",
    features: {
      title: "",
      feature_list: "",
    },
    info_title: "",
    info_content: "",
    uploads: {
      banner: [],
      pictures: [],
    },
  };

  old_pictures: Array<string> = [];

  //@Watch("old_pictures", { deep: true })

  getData() {
    this.isLoading = true;
    axios
      .get("/admin/page/" + this.$route.params.id)
      .then((resp: any) => {
        this.isLoading = false;
        let dd: any = resp.data.data;
        let page: any = JSON.parse(dd.data);
        this.pageSetUpForm.product_name = page.product_name;
        this.pageSetUpForm.short_description = page.short_description;
        this.pageSetUpForm.long_description_title = page.long_description_title;
        this.pageSetUpForm.long_description_content =
          page.long_description_content;
        this.pageSetUpForm.features = page.features;
        this.pageSetUpForm.info_title = page.info_title;
        this.pageSetUpForm.info_content = page.info_content;
        this.old_pictures = page.pictures;
      })
      .catch((err: any) => {
        this.isLoading = false;
        this.$toasted.error("Error fetching page").goAway(1000);
      });
  }

  removePic(index: any){
     let hh = this.old_pictures;
      let pulled = _.pullAt(hh, [index]);
      this.old_pictures = hh;
  }

  mounted() {
    this.getData();
  }

  submitPageSetUpForm() {
    this.$v.pageSetUpForm.$touch();
    this.isFormSubmitting = true;
    //console.log(this.$v.pageSetUpForm);
    if (this.$v.pageSetUpForm.$invalid) {
      this.$v.pageSetUpForm.$touch();
      this.isFormSubmitting = false;
    } else {
      let page = this.$v.pageSetUpForm.$model;
      let formData = new FormData();
      formData.append("_method", "PUT");
      formData.append("page", JSON.stringify(page));
      formData.append("page_banner", page.uploads.banner);
      formData.append("old_pictures", page.uploads.banner);
      _.forEach(page.uploads.pictures, (file, key) => {
        formData.append("page_pictures[]", file);
      });

      axios
        .post("/admin/page/" + this.$route.params.id, formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        })
        .then((resp: any) => {
          this.isFormSubmitting = false;
          this.$router.push({ name: "PageHome" });
          this.$toasted.success("Page updated successfully!").goAway(1000);
        })
        .catch((error) => {
          this.isFormSubmitting = false;
          this.$toasted.error(error.error);
        });
    }
  }

  get productNameError() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.pageSetUpForm.product_name.$dirty) return errors;
    !val.pageSetUpForm.product_name.required &&
      errors.push("Product Name is required.");
    return errors;
  }

  get infoContentError() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.pageSetUpForm.info_content.$dirty) return errors;
    !val.pageSetUpForm.info_content.required &&
      errors.push("Info content is required.");
    return errors;
  }

  get infoTitleError() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.pageSetUpForm.info_title.$dirty) return errors;
    !val.pageSetUpForm.info_title.required &&
      errors.push("Info title is required.");
    return errors;
  }

  get featureListError() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.pageSetUpForm.features.feature_list.$dirty) return errors;
    !val.pageSetUpForm.features.feature_list.required &&
      errors.push("Features list is required.");
    return errors;
  }

  get featureTitleError() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.pageSetUpForm.features.title.$dirty) return errors;
    !val.pageSetUpForm.features.title.required &&
      errors.push("Features title is required.");
    return errors;
  }

  get longDescriptionContentError() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.pageSetUpForm.long_description_content.$dirty) return errors;
    !val.pageSetUpForm.long_description_content.required &&
      errors.push("Long description content is required.");
    return errors;
  }

  get longDescriptionError() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.pageSetUpForm.long_description_title.$dirty) return errors;
    !val.pageSetUpForm.long_description_title.required &&
      errors.push("Long description title is required.");
    return errors;
  }

  get shortDescriptionError() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.pageSetUpForm.short_description.$dirty) return errors;
    !val.pageSetUpForm.short_description.required &&
      errors.push("Short description is required.");
    return errors;
  }

  get headlineErrors() {
    const errors: any = [];
    const valitrons: any = this.$v.pageSetUpForm.headlines;
    _.forEach(valitrons.$each.$iter, (val: any, index: any) => {
      let titleErrors: any = [];
      let taglineErrors: any = [];
      if (!val["title"]["$dirty"]) {
        titleErrors;
      }
      if (val["title"]["$dirty"] && !val["title"]["required"]) {
        titleErrors.push(
          "Headline Title " + (parseInt(index) + 1) + " is required"
        );
      }
      if (!val["tagline"]["$dirty"]) {
        taglineErrors;
      }
      if (val["tagline"]["$dirty"] && !val["tagline"]["required"]) {
        taglineErrors.push(
          "Headline Tagline " + (parseInt(index) + 1) + " is required"
        );
      }

      errors.push({ title: titleErrors, tagline: taglineErrors });
    });
    return errors;
  }

  get featuresErrors() {
    const errors: any = [];
    const valitrons: any = this.$v.pageSetUpForm.features;
    _.forEach(valitrons.$each.$iter, (val: any, index: any) => {
      let titleErrors: any = [];
      let descriptionErrors: any = [];
      if (!val["title"]["$dirty"]) {
        titleErrors;
      }
      if (val["title"]["$dirty"] && !val["title"]["required"]) {
        titleErrors.push(
          "Features Title " + (parseInt(index) + 1) + " is required"
        );
      }
      if (!val["description"]["$dirty"]) {
        descriptionErrors;
      }
      if (val["description"]["$dirty"] && !val["description"]["required"]) {
        descriptionErrors.push(
          "Features Description " + (parseInt(index) + 1) + " is required"
        );
      }

      errors.push({ title: titleErrors, description: descriptionErrors });
    });
    return errors;
  }

  get bannerUploadErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.pageSetUpForm.uploads.banner.$dirty) return errors;
    !val.pageSetUpForm.uploads.banner.required &&
      errors.push("banner is required.");
    return errors;
  }

  get picturesUploadErrors() {
    const errors: Array<string> = [];
    const val: any = this.$v;
    if (!val.pageSetUpForm.uploads.pictures.$dirty) return errors;
    !val.pageSetUpForm.uploads.pictures.required &&
      errors.push("pictures is required.");
    return errors;
  }
}
