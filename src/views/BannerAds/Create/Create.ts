
import {Component, Vue} from "vue-property-decorator";
import { required, minLength } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";

@Component({
  components: {},
  validations: {
    title:{required, minLength: minLength(4)},
    photo: {required}
  }
})
export default class Create extends Vue {
  isLoading = false;
     
  title : any = "" 
  photo : any = ""

  submit(){
    this.$v.$touch();
    this.isLoading = true;
    if (this.$v.$invalid) {
     // this.$v.$touch();
      this.isLoading = false;
    }else{
      let data = this.$v;
    //  console.log(data);
      let formData = new FormData();
      formData.append("title", data.title.$model);
      formData.append("photo", this.photo);
      axios.post("/admin/banner-ads", formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      .then((resp)=>{
        this.$toasted.success("Banner Ads created successfully!").goAway(5000);
        this.isLoading = false;
        this.$router.push({name: "BannerAdsHome"});
      })
      .catch((err)=>{
        this.isLoading = false;
        this.$toasted.error("Error creating brand!").goAway(5000);
      })
    }
  }

  get titleErrors(){
    const errors: Array<string> = [];
      if (!this.$v.title.$dirty) return errors;
      !this.$v.title.minLength &&
          errors.push("title must be at least 4 characters long");
      !this.$v.title.required &&
          errors.push("title is required.");
      return errors;
  }

  get photoErrors() {
    var errors: Array<string> = [];
    if (!this.$v.photo.$dirty) return errors;
    !this.$v.photo.required && errors.push("Photo is required");
    return errors;
  }

}

 