
import {Component, Vue} from "vue-property-decorator";
import { required, minLength } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";

@Component({
  components: {},
  validations: {
    title:{required, minLength: minLength(4)},
    photo: {required}
  }
})

export default class Edit extends Vue {

  isLoading = false;
  isFetching = false;
     
  title : any = "" 
  photo : any = ""
  image : any = ""

  submit(){
    this.$v.$touch();
    this.isLoading = true;
    if (this.$v.$invalid) {
     // this.$v.$touch();
      this.isLoading = false;
    }else{
      let data = this.$v;
    //  console.log(data);
      let formData = new FormData();
      formData.append("title", data.title.$model);
      formData.append("photo", this.photo);
      formData.append("_method", "PUT");
      axios.post("/admin/banner-ads/"+this.$route.params.id, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      .then((resp)=>{
        this.$toasted.success("Banner Ads created successfully!").goAway(5000);
        this.isLoading = false;
        this.$router.push({name: "BannerAdsHome"});
      })
      .catch((err)=>{
        this.isLoading = false;
        this.$toasted.error("Error creating brand!").goAway(5000);
      })
    }
  }

  get titleErrors(){
    const errors: Array<string> = [];
      if (!this.$v.title.$dirty) return errors;
      !this.$v.title.minLength &&
          errors.push("title must be at least 4 characters long");
      !this.$v.title.required &&
          errors.push("title is required.");
      return errors;
  }

  get photoErrors() {
    var errors: Array<string> = [];
    if (!this.$v.photo.$dirty) return errors;
    !this.$v.photo.required && errors.push("Photo is required");
    return errors;
  }

  getData(){
    this.isFetching = true;
    axios.get("/admin/banner-ads/"+this.$route.params.id)
    .then((resp: any)=>{
      let data = resp.data.data
      this.title = data.title;
      this.image = data.image;
      this.isFetching = false;
    })
    .catch((err)=>{
      this.isFetching = false;
      this.$toasted.error("Error fetching banner!").goAway(5000);
    })
  }

  mounted(){
    this.getData();
  }
}

