import { Component, Vue } from "vue-property-decorator";
import store from "@/store";
import Axios from "axios"; 
 
@Component
export default class App extends Vue {
    menu_children_slot_1: object = [
        {
            title: "Surveys",
            icon: "mdi-comment-question-outline",
            children: [
                ["Onboarding Survey", "OnboardingSurveyHome"], //title,route_name,icon
                ["Daily Survey", "DailySurveyHome"], //title,route_name,icon
                ["Feedback Survey", "FeedbackSurveyHome"], //title,route_name,icon
            ]
        },
        {
            title: "Campaigns",
            icon: "mdi-bullhorn-outline",
            children: [
                ["Manage", "CampaignHome"], //title,route_name,icon
                ["Create Campaign", "CreateCampaign"],
                ["Campaign Pages","PageHome"]
            ]
        },
        {
            title: "Banner Ads",
            icon: "mdi-picture-in-picture-bottom-right",
            children: [
                ["Manage", "BannerAdsHome"], //title,route_name,icon
                ["Create Banner Ads", "CreateBannerAds"]
            ]
        },
        {
            title: "Category",
            icon: "mdi-view-list-outline",
            children: [
                ["Manage", "CategoryHome"], //title,route_name,icon
                ["Create Category", "CreateCategory"]
            ]
        },
        {
            title: "Freebie",
            icon: "mdi-heart-outline",
            children: [
                ["Manage", "FreebieHome"], //title,route_name,icon
                ["Create Freebie", "CreateFreebie"]
            ]
        },
        { 
            title: "Outbound Inventory",
            icon: "mdi-inbox-multiple-outline",
            children: [
                ["Outbound Inventory", "InventoryHome"], //title,route_name,icon
            ]
        },
        // { 
        //     title: "Survey",
        //     icon: "mdi-comment-question-outline",
        //     children: [
        //        // ["Create Survey", "CreateSurvey", "mdi-comment-plus"], //title,route_name,icon
        //         ["Onboarding Surveys", "OnboardingSurveys"],
        //         ["Daily Surveys", "DailySurveys"],
        //         ["Feedback Surveys", "FeedbackSurveys"],
        //     ]
        // },
    ];

    menu_children_slot_2: object = [
        {
            title: "Users",
            icon: "mdi-account-group-outline",
            children: [
                ["Consumers", "ConsumerHome"], //title,route_name,icon
                ["Brands", "BrandHome"],
                ["Administrators","AdminHome"]
            ]
        }
    ];

    mini: boolean =  false;

    drawer: any = null;

    minimizeMenu(){
        this.mini = !this.mini;
    }

    toggleMenu(){
        this.drawer = !this.drawer;
    }

    created() {
        var $this = this;
        (<any>this).$http.interceptors.response.use(undefined, function(
            err: any
        ) {
            return new Promise(function(resolve, reject) {
                if (
                    err.status === 401 &&
                    err.config &&
                    !err.config.__isRetryRequest
                ) {
                    $this.$store.dispatch("logout");
                }
                throw err;
            });
        });
        if (this.isLoggedIn) {
            this.fetchUser();
        }
    }

    LogOut() {
        store.dispatch("logout").then(() => {
            this.$toasted
                .success("You have logged out successfully!")
                .goAway(1000);
            this.$router.push("/login");
        });
    }

    fetchUser() {
        Axios.get("/admin/user/profile")
            .then(res => {
                store.commit("set_user", res.data.user);
            })
            .catch(res => {})
            .finally(() => {});
    }

    get isLoggedIn() {
        return store.getters.isLoggedIn;
    }

    get user() {
        return store.getters.user;
    }
}
