import {Component, Vue} from "vue-property-decorator";
import { required, minLength } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";

@Component({
  components: {},
  validations: {
    name:{required, minLength: minLength(4)},
    description:{required, minLength: minLength(4)},
    photo:{required}
  }
})
export default class Create extends Vue {
    isLoading = false;
    
    name : any = ""
    description : any = ""
    photo: any=[];

    submit(){
      this.$v.$touch();
      this.isLoading = true;
      if (this.$v.$invalid) {
       // this.$v.$touch();
        this.isLoading = false;
      }else{
        let data = this.$v;
      //  console.log(data);
        let formData = new FormData();
        formData.append("name",data.name.$model);
        formData.append("description", data.description.$model);
        formData.append("photo", data.photo.$model);
        //console.log(formData);
        axios.post("/admin/brand", formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
        }
        })
        .then((resp)=>{
          this.$toasted.success("Brand created successfully!").goAway(5000);
          this.$router.push({name: "BrandHome"});
        })
        .catch((err)=>{
          this.$toasted.error("Error creating brand!").goAway(5000);
        })
      }
    }

    get nameErrors(){
      const errors: Array<string> = [];
        if (!this.$v.name.$dirty) return errors;
        !this.$v.name.minLength &&
            errors.push("name must be at least 4 characters long");
        !this.$v.name.required &&
            errors.push("name is required.");
        return errors;
    }

    get descriptionErrors(){
      const errors: Array<string> = [];
        if (!this.$v.description.$dirty) return errors;
        !this.$v.description.minLength &&
            errors.push("description must be at least 4 characters long");
        !this.$v.description.required &&
            errors.push("description is required.");
        return errors;
    }

    get photoErrors() {
      const errors: Array<string> = []
      if (!this.$v.photo.$dirty) return errors;
      !this.$v.photo.required &&
          errors.push("photo is required.");
      return errors;
  }


}

