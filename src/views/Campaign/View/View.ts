import { Component, Prop, Vue, Watch } from "vue-property-decorator";
import axios from "axios";
import * as _ from "lodash";
import Spinner from "@/components/Spinner/Spinner.vue";

@Component({
    components: {
        Spinner
    }
})
export default class View extends Vue {
    isLoading: boolean = false;
    campaign: any = {};

    data: any = []; 

    consumer_series: Array<number> = [];
    samples_series: Array<any> = [];

    chartOptions: any = {
        chart: {
            type: "donut"
        },
        labels: ["Female", "Male", "Others"],
        responsive: [
            {
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: "bottom"
                    }
                }
            }
        ],
        colors:['#f5a72b', '#35a9e0', '#7c4d00']
    };

    samplesChartOptions: any = {
        chart: {
            type: "bar"
        },
        xaxis: {
            categories: ['Claimed', 'Matched']
        },
        //labels: ["Claimed", "Matched"],
        responsive: [
            {
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: "bottom"
                    }
                }
            }
        ],
        colors:['#f5a72b', '#35a9e0']
    };


    getCampaign(){
        this.isLoading = true;
        axios.get("/admin/campaign/"+this.$route.params.id)
        .then((resp)=>{
            let data = resp.data.data;
            this.data = data;
            this.isLoading = false;
            this.campaign = data.conditions;
            this.consumer_series.push(this.data.consumers.male, this.data.consumers.female, this.data.consumers.others);
            let kk = this.samples_series.push({
                name: 'series-1',
                data: [this.data.samples.claimed, this.data.samples.matched]
             });
            //this.samples_series.push(this.data.samples.claimed, this.data.samples.matched);

        })
        .catch((err)=>{
            this.isLoading = false;
            this.$toasted.error("error fetching campaign").goAway(1000);
        }) 
    }
  
    mounted(){
        this.getCampaign();
    }
}   
 