import { Component, Prop, Vue } from "vue-property-decorator";
import { required, minLength, sameAs, email } from "vuelidate/lib/validators";
import axios from "axios";
import * as _ from "lodash";
import Spinner from "@/components/Spinner/Spinner.vue";

@Component({
    validations: {
        basicSetUpForm: {
            campaign_name: { required, minLength: minLength(4) },
            brand: { required },
            category: { required }
        },
        pageSetUpForm: {
            time_and_sample: {
                sample: { required },
                duration: { required }
            }
        }
    },
    components: {
        Spinner
    }
})
export default class Edit extends Vue {
    currentStep: number = 1;

    isLoading: boolean = false;
    isFormSubmitting: boolean = false;
    hasError: boolean = false;
    errorMessages: string = "";
    data: any = [];
    initMatchingTagsData : any = [];

    categories: any = [];
    selectedCategories: string = "";
    selectedColor: string = "";

    brands: Array<object> = [];
    matching_tags: Array<object> = [];
    selected_matching_tags: Array<object> = [];
    selected_matching_data: Array<object> = [];

    countries: Array<string> = [];
    states: Array<string> = [];

    gender: Array<string> = [];
    age: Array<string> = [];
    marital_status: Array<string> = [];
    education: Array<string> = [];
    employment: Array<string> = [];

    dateRange: any = ["2019-09-10", "2019-09-20"];
    Datemodal: boolean = false;

    //FORMS
    basicSetUpForm: any = {
        campaign_name: "",
        brand: "",
        category: ""
    };

    pageSetUpForm: any = {
        time_and_sample: {
            sample: 0,
            duration: ["2020-09-10", "2020-09-20"]
        }
    };

    formData: Array<object> = [];


    getTitle(selected: any){
        let dd: any = _.find(this.matching_tags, {id: selected['id']});
        return dd['title'];
    }

    submitBasicSetUpForm() {
        this.$v.basicSetUpForm.$touch();
        let data = this.$v.basicSetUpForm.$model;
        data.category = this.selectedCategories;
        if (this.$v.basicSetUpForm.$invalid) {
            this.$v.basicSetUpForm.$touch();
        } else {
            this.currentStep = 2;
        }
    }

    submitConditionForm() {
        //this.$v.conditionForm.$touch();
        this.$v.conditionForm.$touch();
        let data = this.$v.conditionForm.$model;
        if (this.$v.conditionForm.$invalid) {
            this.$v.conditionForm.$touch();
        } else {
            this.currentStep = 3;
        }
    }

    testUp(){
      //console.log(this.$v.conditionForm.$model);
      this.currentStep = 3;
    }

    submitPageSetUpForm() {
        this.$v.pageSetUpForm.$touch();
        this.isFormSubmitting = true;
        if (this.$v.pageSetUpForm.$invalid) {
            this.$v.pageSetUpForm.$touch();
            this.isFormSubmitting = false;
        } else{
            let page = this.$v.pageSetUpForm.$model;
            let basic = this.$v.basicSetUpForm.$model;
            
            let formData = new FormData();
            formData.append("_method", 'PUT');
            formData.append("brand", basic.brand);
            formData.append("campaign_name", basic.campaign_name);
            formData.append("status", 'OPEN');
            formData.append("starts_on",page.time_and_sample.duration[0]);
            formData.append("ends_on", page.time_and_sample.duration[1]);
            formData.append("categories", basic.category);
            formData.append("matching_tag_data", <any>this.selected_matching_data);
            formData.append("total_products", page.time_and_sample.sample);
            //formData.append("page", JSON.stringify(page));
            // formData.append("page_banner", page.uploads.banner);

            // formData.append("footer_banner", page.footer.banner);
            // _.forEach(page.uploads.pictures, (file, key)=>{
            //     formData.append("page_pictures[]", file);
            // });

           axios.post("/admin/campaign/"+this.$route.params.id, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
           })
           .then((resp: any)=>{
            this.isFormSubmitting = false; 
                this.$router.push({name: "CampaignHome"});
                this.$toasted.success("Campaign created successfully!").goAway(1000);
           })
           .catch((error)=>{
            this.isFormSubmitting = false;
                this.$toasted.error(error.error);
           });
        }
    }

    initPage() { 
        this.isLoading = true;
        axios
            .get("/admin/campaign/load-edit/"+this.$route.params.id)
            .then((resp: any) => {
                //console.log(resp.data.data.brands);
                let data = resp.data.data;
                this.categories = data.categories;
                this.brands = resp.data.data.brands;
                this.matching_tags = data.matching_tags;
                this.isLoading = false;
                this.selectedCategories = this.categories[0].id;
                this.initMatchingTagsData = data;
                //this.selected_matching_tags = data.selected_matching_tags;
                // this.selected_matching_data = data.selected_matching_data;
                this.basicSetUpForm['campaign_name'] = data.campaign.title;
                this.basicSetUpForm['category'] = data.campaign.category_id;
                this.basicSetUpForm['brand'] = data.campaign.brand_id;
                this.pageSetUpForm['time_and_sample']['sample'] = data.campaign.inbound_inventories.quantity;
                //this.pageSetUpForm['time_and_sample']['duration'] = [data.campaign.starts_on, data.campaign.ends_on];

            }) 
            .catch(error => {
                this.isLoading = false; 
                this.$toasted.error("error fetching data!").goAway(1000);
            });
    }

    markedCategory(id: any) {
       // if (_.includes(this.selectedCategories, id)) {
        if (this.selectedCategories = id) {
            return true;
        } else {
            return false;
        }
    }

    toggleMarkCategory(id: any) {
        if (!_.includes(this.selectedCategories, id)) {
            this.selectedCategories = id;
        } else {
            //_.pull(this.selectedCategories, id);
            this.selectedCategories = "";
        }
        this.markedCategory(id);
    }

    mounted() {
        this.initPage();
    } 

    get campaignNameErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.basicSetUpForm.campaign_name.$dirty) return errors;
        !val.basicSetUpForm.campaign_name.minLength &&
            errors.push("campaign_name must be at least 4 characters long");
        !val.basicSetUpForm.campaign_name.required &&
            errors.push("campaign name is required.");
        return errors;
    }

    get brandErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.basicSetUpForm.brand.$dirty) return errors;
        !val.basicSetUpForm.brand.required && errors.push("brand is required.");
        return errors;
    }

    get categoryErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.basicSetUpForm.category.$dirty) return errors;
        !val.basicSetUpForm.category.required &&
            errors.push("category is required.");
        return errors;
    }

    get sampleErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.pageSetUpForm.time_and_sample.sample.$dirty) return errors;
        !val.pageSetUpForm.time_and_sample.sample.required &&
            errors.push("sample quantity is required.");
        return errors;
    }

    get durationErrors() {
        const errors: Array<string> = [];
        const val: any = this.$v;
        if (!val.pageSetUpForm.time_and_sample.duration.$dirty) return errors;
        !val.pageSetUpForm.time_and_sample.duration.required &&
            errors.push("duraron is required.");
        return errors;
    }
}
